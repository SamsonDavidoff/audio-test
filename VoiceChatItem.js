import React, {useState} from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';

import fs from 'react-native-fs';
import Sound from 'react-native-sound';

const VoiceChatItem = ({path, onPlay, onStop, buttonText}) => {
  return (
    <View style={styles.container}>
      <Text style={{flex: 1}}>{path}</Text>
      <View style={{width: 70}}>
        <Button
          title={buttonText}
          onPress={() => {
            if (buttonText === 'Play') {
              onPlay();
            } else {
              onStop();
            }
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default VoiceChatItem;
