import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  FlatList,
  PermissionsAndroid,
} from 'react-native';

import fs from 'react-native-fs';
import SoundRecorder from 'react-native-sound-recorder';
import Sound from 'react-native-sound';

import VoiceChatItem from './VoiceChatItem';
var clip
const App = () => {
  const [buttonText, setButtonText] = useState('Record');
  const [playButtonText, setPlayButtonText] = useState('Play');
  const [data, setData] = useState([]);

  function playClip(fileName) {
    if(!!clip) clip.stop()
    clip = new Sound(fileName, fs.DocumentDirectoryPath, (error) => {
      if (error) {
        console.log('failed to load the sound', error);
        return;
      }
      clip.play((success) => {
        if (success) {
          console.log('successfully finished playing');
        } else {
          console.log('playback failed due to audio decoding errors');
        }
      });
    });
    
  }
  function stopClip() {
    if(!!clip) clip.stop()
    setPlayButtonText('Play');
    console.log('\nstop callback executed from parent screen!');
 
  }
  const renderItem = (item) => {
    return (
      <VoiceChatItem
        path={item}
        buttonText={playButtonText}
        onPlay={() => {
          const fileNameSections = item.split('/');
          const fileName = fileNameSections[fileNameSections.length - 1];
          playClip(fileName)
         
          setPlayButtonText('Stop');
          console.log('\nplay callback executed from parent screen!',clip);
        }}
        onStop={() => {
         stopClip()
          }}
      />
    );
  };

  const startRecord = () => {
    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.RECORD_AUDIO, {
      title: 'Permissions for write access',
      message: 'Give permission to your storage to write a file',
      buttonPositive: 'ok',
    }).then((status) => {
      if (status === PermissionsAndroid.RESULTS.GRANTED) {
        SoundRecorder.start(
          SoundRecorder.PATH_DOCUMENT + `/${Date.now().toString()}.mp4`,
        ).then(function () {
          console.log('\nstarted recording');
        });
      }
    });
  };

  const stopRecord = () => {
    SoundRecorder.stop().then((file) => {
      console.log('\nstopped recording, audio file saved at: ' + file.path);

      const cloneData = [...data];
      cloneData.push(file.path);

      setData(cloneData);
    });
  };

  return (
    <FlatList
      data={data}
      renderItem={({item}) => renderItem(item)}
      keyExtractor={(_, index) => index.toString()}
      ListHeaderComponent={() => (
        <Button
          title={buttonText}
          onPress={() => {
            if (buttonText === 'Record') {
              // start record here...
              setButtonText('Stop Record');
              startRecord();
            } else {
              // stop and save record here...
              setButtonText('Record');
              stopRecord();
            }
          }}
        />
      )}
    />
  );
};

export default App;
